# -*- coding: utf-8 -*-
from __future__ import print_function
try:
    from urllib2 import urlopen
    from urllib import urlencode, quote as urlquote
except ImportError:
    from urllib.parse import quote as urlquote, urlparse, urljoin, unquote as urlunquote, urlencode
    from urllib.request import Request, urlopen
    from urllib.error import HTTPError, URLError

import os, sys

import json
import hashlib
import re
import os
import shutil
import subprocess
import tempfile
import sys
from mwclient.errors import APIError

def category_members (site, cattitle, objects=True):
    cmcontinue = None
    ret = []
    while True:
        if cmcontinue == None:
            # without cmtype you get both pages + subcats
            resp = site.api("query", list="categorymembers", cmtitle=cattitle, cmtype="page", cmlimit=50)
        else:
            # without cmtype you get both pages + subcats
            resp = site.api("query", list="categorymembers", cmtitle=cattitle, cmtype="page", cmlimit=50, cmcontinue=cmcontinue)
        ret.extend([x['title'] for x in resp['query']['categorymembers']])
        if 'continue' in resp:
            cmcontinue = resp['continue']['cmcontinue']
        else:
            break
    if objects:
        # print "converting to page objects ({0})".format(len(ret))
        ret = [site.pages[x] for x in ret]
    return ret

def category_subcats (site, cattitle, objects=True):
    cmcontinue = None
    ret = []
    while True:
        if cmcontinue == None:
            resp = site.api("query", list="categorymembers", cmtitle=cattitle, cmtype="subcat", cmlimit=50)
        else:
            resp = site.api("query", list="categorymembers", cmtitle=cattitle, cmtype="subcat", cmlimit=50, cmcontinue=cmcontinue)
        ret.extend([x['title'] for x in resp['query']['categorymembers']])
        if 'continue' in resp:
            cmcontinue = resp['continue']['cmcontinue']
        else:
            break
    if objects:
        # print "converting to page objects ({0})".format(len(ret))
        ret = [site.pages[x] for x in ret]
    return ret

def category_members_raw (apiurl, cattitle):
    """ For whatever reason, mwclient's category members doens't seem to return results in order, this is a direct use of the API workaround
    Returns list of page titles
    """

    qdata = {
        "action": "query",
        "list": "categorymembers",
        "cmtitle": cattitle,
        "cmsort": "sortkey",
        "cmlimit": 50,
        "cmprop": "ids|title",
        "cmdir": "asc",
        "format": "json"
    }
    ret = []
    while True:
        url = apiurl + "?" + urlencode(qdata)
        f = urlopen(url)
        resp= json.load(f)
        ret.extend([x['title'] for x in resp['query']['categorymembers']])
        if 'continue' in resp:
            qdata['cmcontinue'] = resp['continue']['cmcontinue']
        else:
            break

    return ret

def ensure_valid_wiki_pagename (name):
    """
    Following the guidelines given here:
    https://www.mediawiki.org/wiki/Manual:Page_title
    A little bit unsure how to describe / limit this function
    officially the rules disallow parentheses
    but these seem totally valid from "outside" mediawiki.

    """

    # Base names beginning with a colon (:).
    if name.startswith(":"):
        name = name[1:]

    # Base names equal to "." or "..", or beginning "./" or "../", or containing "/./" or "/../", or ending "/." or "/..".
    if name == "." or name == "..":
        name = ""

    # Base names beginning with a lower-case letter (in any alphabet), depending on the setting of $wgCapitalLinks. Note that a title can be displayed with an initial lower-case letter, using DISPLAYTITLE or the {{lowercase title}} template. This does not fix every occurrence, like the history, edit, or log pages (phab:T55566) - or the browser address bar (phab:T63851), but only affects the page title on the rendered HTML page and tab/window title bars.
    # Titles starting with a lowercase letter are automatically converted to leading uppercase
    if name[0].upper() != name[0]:
        name = name[0].upper() + name[1:]

    # Titles containing the characters # < > [ ] | { } _ (which have special meanings in Wiki syntax), the non-printable ASCII characters 0–31, the "delete" character 127, or HTML character codes such as &amp;. Note that the plus sign + is allowed in page titles, although in the default setup for MediaWiki it is not. This is configured by setting the value of $wgLegalTitleChars in LocalSettings.php.
    # also makes underscores into spaces
    name = re.sub(r"[<>\[\]|\{\}_]", " ", name)

    # Special characters like ( ) & + are translated into their equivalent %-hex notation
    # name = re.sub(r"[\(\)\&\+]", lambda m: urlquote(m.group(0)), name)
    # not sure why parentheses should be replaced
    # since they seem to be valid

    # Base names whose length exceeds 255 bytes. Be aware that non-ASCII characters may take up to four bytes in UTF-8 encoding, so the total number of characters you can fit into a title may be less than 255.
    # Titles beginning with a namespace alias (WP:, WT:, Project:, Image:, on Wikipedia). For example, the name Project:A-Kon is not possible if Project: is set as a namespace alias.
    # Titles beginning with a prefix that refers to another project, including other language Wikipedias, e.g. "fr:" (see Interwiki linking and Interlanguage links). For example, an article about the album "Q: Are We Not Men? A: We Are Devo!" cannot have that exact name, as the "q:" prefix leads to Wikiquote. (The restriction includes the prefixes "w:" and "en:" that refer to English Wikipedia itself. This self-reference restriction does not apply on all projects; for example, Wikiquote supports titles beginning "Q:".)
    # Titles beginning with any non-standard capitalization of a namespace prefix, alias or interwiki/interlanguage prefix, or any of these with a space (underscore) before or after the colon. For example, it is not possible for a title to begin "HELP:", "HeLp:", "Help :" or "Help:_".
    # Titles consisting of only a namespace prefix, with nothing after the colon.
    # Titles beginning or ending with a space (underscore), or containing two or more consecutive spaces (underscores).
    # Titles containing 3 or more consecutive tildes. (~~~)
    # A title can normally contain the character %. However it cannot contain % followed by two hexadecimal digits (which would cause it to be converted to a single character, by percent-encoding).

    return name

def ensure_unique_wiki_pagename (wiki, name):
    # print ("[ensure_unique_wiki_pagename]", name, file=sys.stderr)
    safename = ensure_valid_wiki_pagename(name)
    if safename != name:
        print ("[ensure_unique_wiki_pagename] using", safename, file=sys.stderr)
        name = safename
    if not wiki.pages[name].exists:
        return name
    print ("[ensure_unique_wiki_pagename], trying other names", file=sys.stderr)

    # Parse name in form:
    # base[ (n)][.ext]

    m = re.search(r"^(.+?)(?: \((\d+)\))?(\..+)?$", name)
    base, n, ext = m.groups()
    if n:
        n = int(n) + 1
    else:
        n = 2
    if ext == None:
        ext = ""

    while True:
        name = "{0} ({1}){2}".format(base, n, ext)
        print ("[ensure_unique_wiki_pagename], trying", name, file=sys.stderr)
        if not wiki.pages[name].exists:
            return name
        n += 1

def upload_file_to_wiki (wiki, path, filename=None, description="", phpuploader=None, user=None):
    """ Takes a file path and ensures the file is uploaded to the wiki
    Returns the wiki page object corresponding to the (previous) Image object
    """

    # adding file size limiter, MM Jun2018 (kind of slapped in)
    filesize = os.path.getsize(path)
    if filesize > 10 * 1000 * 1000:
        print ("[upload_file_to_wiki]: File is too large ({0}mb), skipping".format(filesize/(1000*1000)), file=sys.stderr)
        return None
    sh = hashlib.sha1()
    with open(path, "rb") as f:
        while True:
            data = f.read()
            if not data:
                break
            sh.update(data)
    im = None
    for im in wiki.allimages(sha1=sh.hexdigest()):
        print ("[upload_file_to_wiki]: File <{0}> already uploaded to wiki.".format(os.path.basename(path)), file=sys.stderr)
        return im

    if filename == None:
        filename = os.path.basename(path)
    # Ensure filename is unique (prevent clobbering)
    pagename = ensure_unique_wiki_pagename(wiki, "File:"+filename)
    filename = pagename.split(":", 1)[1] # Strip File:

    print ("[upload_file_to_wiki]: uploading {0}".format(path), file=sys.stderr)
    if phpuploader:
        # move the file to a temporary directory
        ppath, fn = os.path.split(path)
        with tempfile.TemporaryDirectory(dir=ppath) as tmpdirname:
            tpath = os.path.join(tmpdirname, filename)
            print('[upload_file_to_wiki]: created temporary directory', tmpdirname, file=sys.stderr)
            print('[upload_file_to_wiki]: copying file to temporary directory', tmpdirname, file=sys.stderr)
            shutil.copy(path, tpath)
            print('[upload_file_to_wiki]: using importImages.php', tmpdirname, file=sys.stderr)
            uargs = ["php", phpuploader, "--skip-dupes", "--comment", description]
            if user != None:
                uargs.append("--user")
                uargs.append(user)
            uargs.append(tmpdirname)
            subprocess.call(uargs)
            # TODO: ADD description / comment with link to containing item?! ... or source
            # TODO: Using tmp.png is going to cause trouble!!!!
            # shutil.move(tpath, path)
    else:
        with open(path, "rb") as f:
            wiki.upload(f, filename, description)

    for im in wiki.allimages(sha1=sh.hexdigest()):
        return im

def upload_url_to_wiki (url, wiki, description="", phpuploader=None, user=None, cachedir=None, filetest=None):
    tmp = None
    try:
        if cachedir == None:
            tmp = tempfile.TemporaryDirectory()
            cachedir = tmp.name
        path = download_url_to_file(url, cachedir)
        if not path:
            return None
        if filetest:
            if not filetest(path):
                return None
        filename = os.path.basename(path)
        filepage = upload_file_to_wiki (wiki, path, filename=filename, description=description, phpuploader=phpuploader, user=user)
        # TODO: delete the path?? (when not using tmpdirectory)
        return filepage
    except APIError as e:
        print ("upload_url_to_wiki: {0}, APIError: {1}".format(url, e), file=sys.stderr)
    finally:
        if tmp:
            tmp.cleanup()

def allpages (site, limit=500):
    apcontinue = None
    while True:
        if apcontinue == None:
            resp = site.api("query", list="allpages", aplimit=limit)
        else:
            resp = site.api("query", list="allpages", aplimit=limit, apcontinue=apcontinue)
        for x in resp['query']['allpages']:
            yield x['title']
        if 'continue' in resp:
            apcontinue = resp['continue']['apcontinue']
        else:
            break

def parse_templates_text (src):
    re.search(r"{{(.+?)}}", re.DOTALL)

if __name__ == "__main__":
    import argparse
    from mwclient import Site
    ap = argparse.ArgumentParser("")
    ap.add_argument("--wikiprotocol", default="http")
    ap.add_argument("--wikihost", default="erg.activearchives.org")
    ap.add_argument("--wikipath", default="/mw/")
    ap.add_argument("--user", default=None)
    ap.add_argument("--password", default=None)
    ap.add_argument("--page", default=None, help="Dump just the page with the given name (otherwise default is to dump all pages)")
    args = ap.parse_args()

    wiki = Site((args.wikiprotocol, args.wikihost), path=args.wikipath)
    # if args.user:
    #     wiki.login(args.user, args.password)
    # if args.page:
    #     page = wiki.pages[args.page]


    # name = "File:Vintimille [live]-Ergote_Radio-vintimille-live.m4a (1)"
    # print (ensure_valid_wiki_pagename(name))



    # url = "http://modedemploi.erg.be/wiki/api.php"
    # print (json.dumps(category_members(url, "Category:Enseignants"), indent=2))

# http://modedemploi.erg.be/wiki/api.php?action=query&list=categorymembers&cmtitle=Category:Enseignants&cmsort=sortkey&cmlimit=50&cmprop=ids|title&cmdir=asc&cmcontinue=page|4c4f52484f0a4a4f414e4e41204c4f52484f|74



# extensions for wget 
EXT = {}
EXT["image/png"] = "png"
EXT["image/gif"] = "gif"
EXT["image/jpeg"] = "jpg"
EXT["image/jpg"] = "jpg"
EXT["image/svg"] = "svg"
EXT["image/svg+xml"] = "svg"
EXT["application/svg+xml"] = "svg"
EXT["application/svg"] = "svg"
EXT["application/pdf"] = "pdf"

def ensure_extension (filename, ext):
    base, fext = os.path.splitext(filename)
    return base+"."+ext

def wget (url, cachedir, blocksize=4*1000, user_agent='Mozilla/5.0'):
    # if type(url) == unicode:
    #     url = url.encode("utf-8")
    try:
        # fin = urlopen(url)
        fin = urlopen(Request(url, headers={'User-Agent': user_agent}))
        ct = fin.info().get("content-type")
        filename = os.path.basename(urlunquote(urlparse(url).path)) or "image"
        if ct in EXT:
            filename = ensure_extension(filename, EXT[ct])
        count = 0
        # path = os.path.join(cachedir, "tmp."+EXT[ct])
        path = os.path.join(cachedir, filename)
        with open(path, "wb") as fout:
            while True:
                data = fin.read(blocksize)
                if not data:
                    break
                fout.write(data)
                count += len(data)
        if count > 0:
            return path

        # print ("[wget] skipping {0} content-type {1}".format(url, ct), file=sys.stderr)
        # return None

    except HTTPError as e:
        print ("[wget]: skipping {0} HTTP ERROR {1}".format(url, e.code), file=sys.stderr)
        return None
    except URLError as e:
        print ("[wget]: skipping {0} URL ERROR {1}".format(url, e.reason), file=sys.stderr)
        return None
    # CATCH ALL EXCEPTIONS (dangerous)
    # except Exception as e:
    #     print ("[wget]: skipping {0} An exception occured {1}".format(url, e), file=sys.stderr)
    #     return None

# def youtube_dl (url, cachedir, format=None, youtube_dl_path="youtube-dl"):
#     args = [youtube_dl_path]
#     if format != None:
#         args.append("-f")
#         args.append(format)
#     args.append(url)
#     p = subprocess.Popen(args, cwd=cachedir, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
#     output, _ = p.communicate()
#     output = output.decode("utf-8")
#     m = re.search(r"^\[download\] Destination: (.+?)\s*$", output, re.M)
#     if m:
#         filename = m.group(1)
#         # print ("DOWNLOADED \"{0}\"".format(filename), file=sys.stderr)
#         return os.path.join(cachedir, filename)
#     else:
#         m = re.search(r"^\[download\] (.+?) has already been downloaded", output, re.M)
#         if m:
#             filename = m.group(1)
#             # print ("ALREADY DOWNLOADED \"{0}\"".format(filename))
#             return os.path.join(cachedir, filename)
#         else:
#             # print ("DOWNLOAD FAILURE", file=sys.stderr)
#             return None

def download_url_to_file (href, cachedir):
    """ returns path to file """
    # if "mixcloud.com/" in href:
    #     print ("[mixcloud] {0}".format(href), file=sys.stderr)
    #     return youtube_dl(href, cachedir)
    # elif "vimeo.com/" in href:
    #     print ("[vimeo] {0}".format(href), file=sys.stderr)
    #     return youtube_dl(href, cachedir, format="http-360p")
    # elif "youtube.com/" in href:
    #     print ("[youtube] {0}".format(href), file=sys.stderr)
    #     return youtube_dl(href, cachedir, format="18")
    # elif "youtu.be/" in href:
    #     print ("[youtube] {0}".format(href), file=sys.stderr)
    #     return youtube_dl(href, cachedir, format="18")
    # else:
    print ("[download_url_to_file]", href, file=sys.stderr)
    return wget(href, cachedir)

