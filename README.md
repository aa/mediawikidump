mediawikidump is a script to dump the pages of a mediawiki to static HTML files.


Requirements
===============

* Python 3.*

python libraries:

* mwclient
* html5lib
* jinja2


Install
============

    pip3 install mwclient html5lib jinja2

    python3 setup.py install


Usage
============

No user credentials (public wiki):

    mediawikidump \
        --wikihost automatist.org \
        --wikipath /mw/ \
    dump --allpages --allcats

With user credentials:

    mediawikidump \
        --wikihost automatist.org \
        --wikipath /mw/ \
        --user WikiUser \
        --password WikiUserPassword \
    dump --allpages --allcats
